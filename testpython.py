import tagger
import timeit

tag = tagger.Tagger()
t1 = timeit.default_timer()
print('\nLoading Data')

tag.load_names('./data/feiba-entities.tsv', './data/feiba-names.tsv')
#tag.load_names('./data/tagger_entities.tsv', './data/tagger_names.tsv')
t2 = timeit.default_timer() - t1

print('\nData Loaded Succesfully in %f Seconds' %t2)
print('\nGetting Matches...\n')

t3 = timeit.default_timer()

documentfile = open('./data/test_documents.tsv')
documenttext = documentfile.read()
documentfile.close()

Doc = '''PMID:25921289|DOI:10.1126/scisignal.2005769	Emdal KB, Pedersen AK, Bekker-Jensen DB, Tsafou KP, Horn H, Lindner S, Schulte JH, Eggert A, Jensen LJ, Francavilla C, Olsen JV	Sci Signal. 8(374):ra40	2015	title	Prothrombin complex concentrate (PCC) products are emerging as alternative strategies for reversing anticoagulant pharmacotherapy. Factor eight inhibitor bypassing activity (FEIBA, or anti-inhibitor coagulant complex) is an activated PCC (aPCC). Although FEIBA is approved by the FDA to control spontaneous bleeding episodes and to prevent bleeding with surgical interventions in hemophilia A and hemophilia B patients with inhibitors to factor VIII, recent data have suggested that the product may be used off-label as an anticoagulant-reversal agent. To evaluate the safety and efficacy of aPCC products in reversing anticoagulant pharmacotherapy, we searched online databases for English-language publications that discussed this topic TIMP-1 increases expression and phosphorylation of proteins associated with drug resistance in breast cancer cells. .
pmid:2345	fjngj	sci	2015	Although FEIBA is approved by the FDA to control spontaneous bleeding episodes and to prevent bleeding with surgical interventions in hemophilia A and hemophilia B patients with inhibitors to factor VIII, recent data have suggested that the product may be used off-label as an anticoagulant-reversal agent
PMID:23909892|DOI:10.1021/pr400457u	Hekmat O, Munk S, Fogh L, Yadav R, Francavilla C, Horn H, Wurtz SOe, Schrohl AS, Damsgaard B, Roemer MU, Belling KC, Jensen NF, Gromova I, Bekker-Jensen DB, Moreira JM, Jensen LJ, Gupta R, Lademann U, Brunner N, Olsen JV, Stenvang J	J Proteome Res. 12(9):4136-51	2013	TIMP-1 increases expression and phosphorylation of proteins associated with drug resistance in breast cancer cells. 	Tissue inhibitor of metalloproteinase 1 (TIMP-1) is a protein with a potential biological role in drug resistance. To elucidate the unknown molecular mechanisms underlying the association between high TIMP-1 levels and increased chemotherapy resistance, we employed SILAC-based quantitative mass spectrometry to analyze global proteome and phosphoproteome differences of MCF-7 breast cancer cells expressing high or low levels of TIMP-1. In TIMP-1 high expressing cells, 312 proteins and 452 phosphorylation sites were up-regulated. Among these were the cancer drug targets topoisomerase 1, 2A, and 2B, which may explain the resistance phenotype to topoisomerase inhibitors that was observed in cells with high TIMP-1 levels. Pathway analysis showed an enrichment of proteins from functional categories such as apoptosis, cell cycle, DNA repair, transcription factors, drug targets and proteins associated with drug resistance or sensitivity, and drug transportation. The NetworKIN algorithm predicted the protein kinases CK2a, CDK1, PLK1, and ATM as likely candidates involved in the hyperphosphorylation of the topoisomerases. Up-regulation of protein and/or phosphorylation levels of topoisomerases in TIMP-1 high expressing cells may be part of the mechanisms by which TIMP-1 confers resistance to treatment with the widely used topoisomerase inhibitors in breast and colorectal cancer..'''

Doc = documenttext

matches = tag.get_matches(Doc, '01' ,[-1,9606] )# './data/feiba-types')
#matches = tag.get_matches(documenttext, '01' ,[-1,9606] )# './data/feiba-types')
t4 = timeit.default_timer() - t3

print('\nMatching Completed! The Process took %f Seconds\n\n' %t4)

print('Start Position\tEnd Position\tEntity Name\tCategory\tIdentifier')

for match in matches:
	txt = Doc[match[0]:match[1]+1]
	for ent in match[2]:
		print (match[0], '\t\t',match[1],'\t\t',)
		print (txt,'\t\t') 
		print (ent[0],'\t\t')
		print (ent[1])

print('\n')
'''
doc = tag.get_entities(Doc, '01',[-1,9696])

print('\n\n:XML OUTPUT')

print(doc) '''

doc_tsv = tag.get_entities(Doc, '01',[-1], format='tsv')

print('TSV OUTPUT Category = -1\n')
print(doc_tsv)


print('\n')

doc_tsv = tag.get_entities(Doc, '01',[9696], format='tsv')

print('TSV OUTPUT Catagory = 9606 \n')
print(doc_tsv)


print('\n')
#html = tag.create_html( Doc, '01', matches)

#print(html)
#print('\n')

#json = tag.get_jsonld(Doc,'utf8' , '01',None, [-1])

#print(json)
#print('\n\n')
