from fastapi import FastAPI,Request
import tagger
import timeit
import configparser

config = configparser.RawConfigParser()
config.read('./data/config.cfg')

input_details = dict(config.items('input_files'))

names_file = input_details['names']
entities_file = input_details['entities']
types_file = input_details['types']
local_file = input_details['human']
global_file = input_details['global']
prefered_names_file = input_details['prefered_names']

def load_file(fname):
        file1 = open(fname,'r')
        data_raw = file1.read().rstrip('\n')
        data_lines = data_raw.split("\n")

        ent = {}

        for lines in data_lines:
                tmp = lines.split('\t')
                ent[tmp[0]]=tmp[1]
    
        return ent

def load_types():
        f = open(types_file,'r')
        lines = f.readlines()
        types = []
        for line in lines:
                tmp = line.strip().split('\t')
                types.append(int(tmp[0]))

        print(len(types))
        return types

def parse_results(matches,types,prefered_names,query_text):
        tagger_results =[]
        for match in matches:
                matched_word = query_text[match[0]:match[1]+1]
                for ent in match[2]:
                        temp = {}
                        temp['start'] = match[0]
                        temp['end'] = match[1]
                        temp['preferred-name'] = prefered_names[ent[1]]
                        temp['id'] = ent[1]
                        temp['semantic-types'] = types[str(ent[0])]
                        temp['matched-words'] = matched_word
                        tagger_results.append(temp)
        return tagger_results


app = FastAPI()
tag = tagger.Tagger()

print('\nLoading Data')
t1 = timeit.default_timer()
tag.load_names(entities_file, names_file)
print('Names and Entities Data loaded..')
tag.load_global(global_file)
print('Global Data is loaded')
#tag.load_local(local_file)
print('Local Data is loaded')

types = load_types()
prefered_names = load_file(prefered_names_file)
types_dict = load_file(types_file)
t2 = timeit.default_timer() - t1

print('\nData Loaded Succesfully in %f Seconds' %t2)


@app.get("/")
async def root(query_text:str):
        t2 = timeit.default_timer()
        matches = tag.get_matches(query_text, '01' ,types )
        #matches = tag.get_matches(s, '01' ,[-1,-2,-3,-11,-21,-22,-23,-24,-25,-26,-27,-28,-29,-30,-31,-36,9606] )
        t4 = timeit.default_timer() - t2

        print('\nMatching Completed! The Process took %f Seconds\n\n' %t4)
        print(matches)
        tagger_results = parse_results(matches,types_dict,prefered_names,query_text)
        return tagger_results